# Projeto de Descriptografia de Imagens - PPM e PGM


#### Compilação

No terminal, ir para o diretório raiz do projeto e executar o comando:
```sh
$ make clean
```

Concluído isso, executar o comando, que irá compilar todos os arquivos necessários para execução do Software:
```sh
$ make
```


#### Execução

Para iniciar o Software, digite o comando:
```sh
$ make run
```

Como pedido inicialmente, digite o caminho do onde está a imagem. Caso este esteja no diretório raiz do projeto, basta inserir somente o nome da imagem.


Caso não possua imagens para executar o Software, basta usar as que estão na pasta **img**, digitando, por exemplo:
```sh
img/exemplo1.ppm
```

Se a imagem for encontrada, aberta e seu tipo for aceito, o Software exibirá a próxima linha. Caso contrário, exibirá a seguinte mensagem:
`Arquivo incompatível com o Software!` 


O Software solicitará ao usuário se o mesmo deseja descobrir a mensagem contida na imagem, caso o mesmo deseje basta digitar o seguinte caracter literal, e a mensagem será exibida em seguida:
```sh
S
```


Após exibida a mensagem, o Software perguntará se o usuário deseja fazer leitura de uma nova imagem, caso o mesmo deseje basta digitar o caracter literal: 
```sh
S
```

Caso haja algum problema com a abertura do arquivo o Software exibirá a mensagem `Impossível abrir arquivo`.

Ao final da execução do Software, estará salvo na pasta **img** as ultimas imagens lidas de cada tipo.