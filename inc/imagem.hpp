#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <string>
#include <fstream>

using namespace std;

class Imagem{
private:
	int linhas;
	int colunas;
	int tonalidade;
	int inicio_mensagem;
	int tamanho_mensagem;
	string codigo;

protected:
	fstream arquivo;
	unsigned char * pixels;

public:
	Imagem(string caminho);
	~Imagem();
	void setLinhas(int linhas);
	int getLinhas();
	void setColunas(int colunas);
	int getColunas();
	void setTonalidade(int tonalidade);
	int getTonalidade();
	void setCodigo(string codigo);
	string getCodigo();
	void setInicio_mensagem(int inicio_mensagem);
	int getInicio_mensagem();
	void setTamanho_mensagem(int tamanho_mensagem);
	int getTamanho_mensagem();
	void readPixels();
	void readPixels(int dimensao);
};

#endif