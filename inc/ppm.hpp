#ifndef PPM_HPP
#define PPM_HPP

#include "imagem.hpp"

class PPM : public Imagem{
private:
	string keyword;
	string mensagem;

public:
	PPM(string caminho);
	~PPM();
	void savePPM();
	string getMensagem();
};

#endif