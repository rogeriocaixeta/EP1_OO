#include <iostream>
#include "imagem.hpp"

using namespace std;

Imagem::Imagem(string caminho){
	arquivo.open(caminho);
	char aux;
	if (! arquivo.is_open() ){
		cout << "Impossível abrir arquivo" << endl;
		arquivo.clear();
		arquivo.close();
	}
	else {
		getline(arquivo, codigo);
		//Para o ponteiro do arquivo pular o caracter "#"
		arquivo.ignore(256, '#');
		arquivo >> inicio_mensagem >> tamanho_mensagem;
		arquivo.get(aux);
	}
}

Imagem::~Imagem(){
	arquivo.close();
}

void Imagem::setLinhas(int linhas){
	this->linhas = linhas;
}

int Imagem::getLinhas(){
	return linhas;
}

void Imagem::setColunas(int colunas){
	this->colunas = colunas;
}

int Imagem::getColunas(){
	return colunas;
}

void Imagem::setTonalidade(int tonalidade){
	this->tonalidade = tonalidade;
}

int Imagem::getTonalidade(){
	return tonalidade;
}

void Imagem::setCodigo(string codigo){
	this->codigo = codigo;
}

string Imagem::getCodigo(){
	return codigo;
}

void Imagem::setInicio_mensagem(int inicio_mensagem){
	this->inicio_mensagem = inicio_mensagem;
}

int Imagem::getInicio_mensagem(){
	return inicio_mensagem;
}

void Imagem::setTamanho_mensagem(int tamanho_mensagem){
	this->tamanho_mensagem = tamanho_mensagem;
}

int Imagem::getTamanho_mensagem(){
	return tamanho_mensagem;
}

void Imagem::readPixels(){
	pixels = new unsigned char[ (getLinhas() * getColunas()) ];
	char aux;

	arquivo.ignore(256, '\n');
	for (int x = 0; x < ( getLinhas() * getColunas() ); x++){
		arquivo.get(aux); 
		pixels[x] = aux;
	}
}

void Imagem::readPixels(int dimensao){
	pixels = new unsigned char[ (getLinhas() * getColunas() * dimensao) ];
	char aux;

	arquivo.ignore(256, '\n');
	for (int x = 0; x < ( getLinhas() * getColunas() * dimensao ); x++){
		arquivo.get(aux); 
		pixels[x] = aux;
	}
}