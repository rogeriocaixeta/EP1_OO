#include <iostream>
#include <string>
#include <stdlib.h>
#include "imagem.hpp"
#include "pgm.hpp"
#include "ppm.hpp"

using namespace std;

int main(int argc, char **argv){
	string caminho = (""), opcao = ("");
	fstream ponteiro;
	char escolha;

	do {
		system("clear");
		cout << "Bem vindo ao Descriptador de Imagens! - Versão 1.0" << endl << endl << endl;
		cout << "Digite o caminho da imagem: ";
		cin >> caminho;
		ponteiro.open(caminho);
		ponteiro >> opcao;
		ponteiro.close();

		if (opcao == "P5"){
			PGM *imagem = new PGM(caminho);
			char escolha;

			cout << endl << endl; 
			cout << "Sabia que existe uma mensagem escondida neste arquivo?!" << endl << endl << "Deseja saber qual a mensagem (S/N)? ";
			cin >> escolha;
			cout << endl;
			if (escolha == 'S'){
				cout << "Essa é a mensagem: " << imagem->getMensagem() << endl << endl;
			}
			
	
			delete imagem; 
		}

		else if (opcao == "P6")
		{
			PPM *imagem = new PPM(caminho);

			cout << endl << endl; 
			cout << "Sabia que existe uma mensagem escondida neste arquivo?!" << endl << endl << "Deseja saber qual a mensagem (S/N)? ";
			cin >> escolha;
			cout << endl;
			if (escolha == 'S'){
				cout << "Essa é a mensagem: " << imagem->getMensagem() << endl << endl;
			}

			delete imagem;

		}

		else {
			cout << endl << endl << "Arquivo incompatível com o Software!" << endl << endl;
		}

		cout << endl << "Deseja abrir uma nova imagem? (S/N): ";
		cin >> escolha;

	} while (escolha == 'S');

	system("clear");
	return 0;
}