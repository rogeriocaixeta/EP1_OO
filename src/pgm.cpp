#include <iostream>
#include "pgm.hpp"

using namespace std;

PGM::PGM(string caminho) : Imagem::Imagem(caminho){
	int colunas, linhas, tonalidade;

	arquivo >> deslocador;
	arquivo.ignore(256, '\n');
	arquivo >> colunas >> linhas >> tonalidade;
	setColunas(colunas);
	setLinhas(linhas);
	setTonalidade(tonalidade);

	readPixels();
}

PGM::~PGM(){
	savePGM();
	delete pixels;
}

void PGM::savePGM(){
	ofstream new_arquivo;

	new_arquivo.open("img/imagem.pgm");

	new_arquivo << getCodigo() << endl << "#" << getInicio_mensagem() << " " << getTamanho_mensagem();
	new_arquivo << " " << deslocador << endl << getColunas() << " " << getLinhas() << endl << getTonalidade() << endl;

	for (int x = 0; x < (getLinhas()*getColunas()); x++){
		new_arquivo << pixels[x];
	}
}

string PGM::getMensagem(){
	char aux;
	for (int i = getInicio_mensagem(); i < (getTamanho_mensagem() + getInicio_mensagem()); i++){
		if (pixels[i] >= 'A' && pixels[i] <= 'Z')
		{
			aux = pixels[i] - deslocador;
			if (aux <= 'A')
			{
				//Para tornar o alfabeto cíclicio
				aux = aux + 'Z' - 'A' + 1;
			}
			mensagem += aux;
		}
		else if (pixels[i] >= 'a' && pixels[i] <= 'z')
		{
			aux = pixels[i] - deslocador;
			if (aux < 'a')
			{
				//Para tornar o alfabeto cíclicio
				aux = aux + 'z' - 'a' + 1;
			}
			mensagem += aux;
		}
		else 
		{
			mensagem += pixels[i];
		}
	}

	return mensagem;
}


