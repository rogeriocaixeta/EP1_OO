#include <bits/stdc++.h>
#include "ppm.hpp"

using namespace std;

PPM::PPM(string caminho) : Imagem::Imagem(caminho){
	int colunas, linhas, tonalidade;

	getline(arquivo, keyword);
	arquivo >> colunas >> linhas >> tonalidade;
	setColunas(colunas);
	setLinhas(linhas);
	setTonalidade(tonalidade);

	readPixels(3);
}

PPM::~PPM(){
	savePPM();
	delete pixels;
}

void PPM::savePPM(){
	ofstream new_arquivo;

	new_arquivo.open("img/imagem.ppm");

	new_arquivo << getCodigo() << endl << "#" << getInicio_mensagem() << " " << getTamanho_mensagem();
	new_arquivo << " " << keyword << endl << getColunas() << " " << getLinhas() << endl << getTonalidade() << endl;

	for (int x = 0; x < (getLinhas()*getColunas()*3); x++){
		new_arquivo << pixels[x];
	}
}

string PPM::getMensagem(){
	string enconder;
	bool naotem = true;
	unsigned int i, j;
	int c, aux;

	//Inserindo Keyword no meu alfabeto cifrado e checando se existe repetição
	for (i = 0; i < keyword.size(); i++){
		for (j = 0; j < enconder.size(); j++){
			if (keyword[i] == enconder[i]){
				naotem = false;
			}
		}

		if (naotem){
			if (keyword[i] >= 'a' && keyword[i] <= 'z')
				//Convertendo letras minúsculas em maiúsculas
				enconder += keyword[i] - 32;
			else
				enconder += keyword[i];
		}
		naotem = true;
	}

	//Inserindo resto do alfabeto
	for (i = 0; i < 26; i++){
		for (j = 0; j < enconder.size(); j++){
			if (enconder[j] == ('A' + (int)i)){
				naotem = false;
			}
		}

		if (naotem){
			//Inserindo caracteres a partir da tabela ASC II
			enconder += ('A' + i);
		}
		naotem = true;
	}

	for (c = getInicio_mensagem(); c < (getInicio_mensagem() + 3 * getTamanho_mensagem()); c+=3){
		aux = (pixels[c] % 10) + (pixels[c + 1] % 10) + (pixels[c + 2] % 10);

		// Loop limitado pelo tamanho do alfabeto
		for (i = 0; i < 26; i++){
			if (aux == 0){
				mensagem += " ";
				break;
			}
			else if ( ('@' + aux) == enconder[i]){
				//Inserindo caracteres a partir da tabela ASC II
				mensagem += 'A' + i;
				break;
			}
				
		}
	
	} 

	return mensagem;
}